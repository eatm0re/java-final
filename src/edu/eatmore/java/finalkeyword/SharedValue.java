package edu.eatmore.java.finalkeyword;

public class SharedValue {

  public final int value;

  public SharedValue() throws InterruptedException {
    System.out.println("Creating SharedValue");
    SharedContainer.sharedValue = this;  // leaking this
    System.out.println("SharedValue: \"this\" leaked");
    System.out.println("SharedValue: waiting 10 seconds");
    Thread.sleep(10000);
    System.out.println("SharedValue: initializing");
    value = 42;
    System.out.println("SharedValue: initialized");
  }

}
