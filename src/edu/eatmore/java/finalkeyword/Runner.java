package edu.eatmore.java.finalkeyword;

public class Runner {

  public static void main(String[] args) throws InterruptedException {

    Thread writerThread = new Thread(() -> {
      try {
        new SharedValue();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    });

    System.out.println("Running writer thread");
    writerThread.start();
    System.out.println("Waiting 4 seconds");
    Thread.sleep(4000);
    System.out.println("Reading shared value");
    System.out.println(SharedContainer.sharedValue.value);
    System.out.println("Waiting 10 seconds");
    Thread.sleep(10000);
    System.out.println("Reading shared value");
    System.out.println(SharedContainer.sharedValue.value);
  }

}
